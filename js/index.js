$(function(){
    $("[data-toggle = 'tooltip']").tooltip(); //se asigna la funcion tooltips() a todos los data-toggle con el valor 'tooltip'
    $("[data-toggle = 'popover']").popover();
    $('.carousel').carousel({
      interval: 2500
    })
  })
  $('#contacto').on('show.bs.modal', function(e){
    console.log('Se muestra el modal')
  })
  $('#contacto').on('shown.bs.modal', function(e){
    console.log('Se mostro el modal')
    $('.button-modal').removeClass('btn-outline-info')
    $('.button-modal').addClass('btn-outline-danger')
    $('.button-modal').prop('disabled', true)
    
  })
  $('#contacto').on('hide.bs.modal', function(e){
    console.log('Se oculta el modal')
  })
  $('#contacto').on('hidden.bs.modal', function(e){
    console.log('Se oculto el modal')
    $('.button-modal').removeClass('btn-outline-danger')
    $('.button-modal').addClass('btn-outline-info')
    $('.button-modal').prop('disabled', false)
  })